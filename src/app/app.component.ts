import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'All Users', url: '/user', icon: 'people' },
    { title: 'Add User', url: '/user/add', icon: 'add' },
  ];
  constructor() {}
}
