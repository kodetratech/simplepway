import { Injectable } from '@angular/core';
import {
  AngularFireDatabase,
  AngularFireList,
} from '@angular/fire/compat/database';
import User from '../models/user';
@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  userListRef: AngularFireList<User>;
  private dbPath = '/users';
  constructor(private db: AngularFireDatabase) {
    this.userListRef = db.list(this.dbPath);
  }

  getAll(): AngularFireList<User> {
    return this.userListRef;
  }

  createUser(user: User) {
    return this.userListRef.push(user);
  }
  update(key: string, value: any): Promise<void> {
    return this.userListRef.update(key, value);
  }

  delete(key: string): Promise<void> {
    return this.userListRef.remove(key);
  }

  deleteAll(): Promise<void> {
    return this.userListRef.remove();
  }
}
