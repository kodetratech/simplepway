import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import User from 'src/app/models/user';
import { FirebaseService } from 'src/app/services/firebase.service';
@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss'],
})
export class ListUserComponent implements OnInit {
  public userList: Array<User> = [];
  constructor(private firebaseService: FirebaseService) {}

  ngOnInit() {
    this.firebaseService
      .getAll()
      .snapshotChanges()
      .pipe(
        map((changes) =>
          changes.map((c) => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe((data: User[]) => {
        console.log('user data', data);
        this.userList = data;
      });
  }
}
