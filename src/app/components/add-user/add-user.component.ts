import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import User from 'src/app/models/user';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  user: User;
  isSubmitted = false;
  ionicForm: FormGroup;
  constructor(
    private firebaseService: FirebaseService,
    public formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.ionicForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'),
        ],
      ],
    });
  }

  saveUser(): void {
    this.isSubmitted = true;
    if (this.ionicForm.valid) {
      console.log('this user', this.ionicForm.value);
      this.user = new User();
      this.user.name = this.ionicForm.value.name;
      this.user.email = this.ionicForm.value.email;
      this.firebaseService.createUser(this.user).then(() => {
        console.log('Created new item successfully!');
        this.isSubmitted = false;
        this.ionicForm.reset();
      });
    } else {
      console.log(this.ionicForm.value);
    }
  }

  get errorControl() {
    return this.ionicForm.controls;
  }
}
