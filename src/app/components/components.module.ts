import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AddUserComponent } from './add-user/add-user.component';
import { CardComponent } from './card/card.component';
import { ListUserComponent } from './list-user/list-user.component';

@NgModule({
  declarations: [CardComponent, AddUserComponent, ListUserComponent],
  imports: [CommonModule, IonicModule, FormsModule, ReactiveFormsModule],
  exports: [CardComponent],
})
export class ComponentsModule {}
