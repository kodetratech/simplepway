# Install Firebase CLI

npm install -g firebase-tools

## Deploy to Firebase Hosting

firebase login

firebase login --reauth

firebase init hosting

firebase deploy

## Build For Prod Env

ionic build --prod --release
